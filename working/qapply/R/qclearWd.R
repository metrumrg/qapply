qclearWd <- function(workDir, tag){
  if( is.null(tag) ){
    dirs <- paste0(workDir,"/",c("err","log","out"))
    svndirs <- file.path(dirs,".svn")
    dirs <- dirs[!dir.exists(svndirs)]
    for(dir in dirs) unlink(dir,recursive=T)
    unlink(file.path(workDir, paste(tag, "-common.Rdata", sep="")))
    unlink(file.path(workDir, paste(tag, "-libs.Rdata", sep="")))            
    }else{
      dirs <- paste0(workDir,"/",c("err","log","out"),"/",tag)
      svndirs <- file.path(dirs,".svn")
      dirs <- dirs[!dir.exists(svndirs)]
      for(dir in dirs) unlink(dir,recursive=T)
      unlink(file.path(workDir, paste(tag,"common.Rdata", sep="-")))
      unlink(file.path(workDir, paste(tag,"libs.Rdata", sep="-")))      

      # Delete parent dir if empty
      dirs <- dirname(dirs)
      dirs <- dirs[sapply(dirs, function(d) length(list.files(d,recursive=T))==0)]
      for(dir in dirs) unlink(dir, recursive=T)
    }
  unlink(file.path(workDir, "qcontrol.R"))
}
